# Changelog

## 1.0.0

- Rebased config (to the pylint v2.17.4 version from the official github repository) to be compatible with pylint >= v2.17.
- Set minimum supported Python version to 3.11.4.
- Removed following checks from the disabled-list: `bad-continuation`, `too-many-instance-attributes`, `logging-format-interpolation`, `logging-fstring-interpolation`.

## 0.8.0

- Added `df` as allowed variable name.

## 0.7.0

- Disabled `consider-using-f-string` in `.pylintrc`.

## 0.6.0

- Added `ignored-argument-names` to ignore `*args` and `**kwargs`.

## 0.5.0

- Updated `no-docstring-rgx` to allow private methods to not require a docstring.
- Increased `min-similarity-lines` from 10 to 20 to not get so many false positive duplicate code errors anymore.

## 0.4.0

- Disabled `logging-fstring-interpolation` in `.pylintrc`.

## 0.3.0

- Disabled `logging-format-interpolation` in `.pylintrc`.

## 0.2.3

- Removed `id` as good variable name in normal pylintrc, keep it in pylintrc for django.

## 0.2.2

- Added id as a good variable name

## 0.2.1

- From now on not all formatting is ignored by pylint but only `bad-continuation`. This
  way some issues like too long lines which black sometimes can't avoid get linted as
  well.
- Disabled `bad-builtin`

## 0.2.0

- Changed allowed single char variable names.
  The following are allowed from now on: `_`, `c`, `k` ,`n`,`p`,`x`,`X`,`y`

## 0.1.1

- Added version-info in `.pylintrc` file.

## 0.1.0

- Added initial `.pylintrc` file, readme and changelog.
