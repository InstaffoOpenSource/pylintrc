# .pylintrc Repository

This repository contains the `.pylintrc` file used for python projects at Instaffo.
The purpose of of this repository is to have a central place where changes can be
proposed and eventually implemented.